#
# Node container configured for Laravel
#

FROM node:alpine

LABEL description="This image is used for running Node.js software as needed by Laravel." 
LABEL vendor="Wysiwyg Oy" 
LABEL maintainer="Jarno Antikainen <jarno.antikainen@wysiwyg.fi>"

RUN apk add --no-cache autoconf automake bash build-base libpng-dev libtool nasm openssl python3

VOLUME /project
WORKDIR /project
