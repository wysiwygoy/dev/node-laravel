# Image for running Node.js command line commands

Includes the stuff required by Laravel.

## Building

Building Docker image:

```bash
docker build -t registry.gitlab.com/wysiwygoy/dev/node-laravel .
```
